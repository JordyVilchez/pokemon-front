import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { RESOURCE_CACHE_PROVIDER } from '@angular/platform-browser-dynamic';

@Injectable()
export class PokemonService {
  public headers = new Headers();
  public urlpersonas: string = "http://localhost:8080/prueba/obtenerPokemon";


  constructor(private httpClient: HttpClient, public _http: Http) {
    this.headers.append("Content-Type", "application/json");
  }

  protected obtenerHeaders(): Headers {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('Acces-Control-Allow-Headers', 'Content-Type');
    return headers;
  }

  obtenerTodosPokemon(url: string) {
    return this.httpClient.get(url)
  }

  obtenerDetallePokemon(url: string) {
    let queryParams = new URLSearchParams()
    let headers = this.obtenerHeaders();
    headers.append('requestUrl', url);
    let options = new RequestOptions({
      headers: headers,
      search: queryParams
    });

    console.log(url)
    return this._http.get('http://localhost:8080/prueba/obtenerDetallexPokemon', options).map((res: Response) => res.json());

  }

  obtenerDetallePokemon2(url: string) {
    let queryParams = new URLSearchParams()
    let headers = this.obtenerHeaders();
    headers.append('requestUrl', url);
    let options = new RequestOptions({
      headers: headers,
      search: queryParams
    });

    console.log(url)
    return this._http.get('http://localhost:8080/prueba/obtenerDetallexPokemon2', options).map((res: Response) => res.json());

  }
}
