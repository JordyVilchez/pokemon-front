import { PokemonService } from './../pokemon.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})

export class PokemonComponent implements OnInit {

  results: any;
  pree: string;
  listaTmp: any = [];
  nuPagina: number = 0;
  nuRegisMostrar: number = 10;

  constructor(private pokemonService: PokemonService) {
  }

  ngOnInit() {
    this.pokemonService.obtenerTodosPokemon('http://localhost:8080/prueba/obtenerPokemon').subscribe((res: any) => {
      this.results = res.results;
      for (let i = this.nuPagina * this.nuRegisMostrar; i < (this.nuPagina * this.nuRegisMostrar) + this.nuRegisMostrar; i++) {
        this.listaTmp.push(this.results[i]);
      }
      console.log(this.listaTmp);
    });
  }


  public paginacion(nuPagina: number) {
    this.listaTmp = [];
    for (let i = nuPagina * this.nuRegisMostrar; i < (nuPagina * this.nuRegisMostrar) + this.nuRegisMostrar; i++) {
      this.listaTmp.push(this.results[i]);
    }
    console.log(this.listaTmp);
  }

  public paginacionAdelante() {
    if (this.nuPagina >= 89) {
      return;
    }
    this.nuPagina = this.nuPagina + 1;
    console.log(this.nuPagina);
    this.listaTmp = [];
    for (let i = this.nuPagina * this.nuRegisMostrar; i < (this.nuPagina * this.nuRegisMostrar) + this.nuRegisMostrar; i++) {
      if (i >= 893) {
        return;
      }
      this.listaTmp.push(this.results[i]);
    }
    console.log(this.listaTmp);
  }

  public paginacionAtras() {
    if (this.nuPagina <= 0) {
      return;
    }
    this.nuPagina = this.nuPagina - 1;
    console.log(this.nuPagina);
    this.listaTmp = [];
    for (let i = this.nuPagina * this.nuRegisMostrar; i < (this.nuPagina * this.nuRegisMostrar) + this.nuRegisMostrar; i++) {
      this.listaTmp.push(this.results[i]);
    }
    console.log(this.listaTmp);
  }

  public detalle(url, index) {
    this.pokemonService.obtenerDetallePokemon(url).subscribe((res: any) => {
      if (this.listaTmp[index]["detalle"]) {
        delete this.listaTmp[index]["detalle"];
      } else {
        this.pokemonService.obtenerDetallePokemon2(res.species.url).subscribe((detalle2: any) => {
          if (detalle2.evolves_from_species != null) {
            this.pree = detalle2.evolves_from_species.name;
          } else { this.pree = "-"; }
          this.listaTmp[index]["detalle"] = { id: res.id, habilidades: res.abilities, preevolucion: this.pree, capture_rate: detalle2.capture_rate, base_happiness: detalle2.base_happiness }
        });
      }
    });

  }


}